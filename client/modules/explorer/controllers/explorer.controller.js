'use strict';
/**
 * @ngdoc function
 * @name dashboard.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dashboard
 */
angular.module('app.explorer')
  .controller('ExplorerCtrl', ['$scope', '$rootScope', '$meteor', '$location', 'ExplorerService', ExplorerCtrl ])
  .value('googleChartApiConfig', {
    version: '1.1',
    optionalSettings: {
      packages: ['line', 'bar'],
      language: 'en'
    }
  });;


function ExplorerCtrl($scope, $rootScope, $meteor, $location, ExplorerService) {
  // Possible input values
  $scope.strokes = ['Freestyle', 'Backstroke', 'Breaststroke', 'Butterfly' ];
  $scope.distances = [50, 100, 200, 400, 500, 1000, 1650];
  $scope.genders = ['Male', 'Female'];

  // we will store all of our form data in this object
  $scope.formData = {
    "stroke": 'Freestyle',
    "distance": 100,
    'gender': 'Male',
    'time': '45',
    'division': 'I',
  };
  // Subscriptions
  $meteor.subscribe('teams');

  // $scope.states @ bottom
  $scope.teams = [];
  $scope.explorerStarted = false;
  /** True when Search Button pressed */
  $scope.searched = false;
  /** True when querying server */
  $scope.loading = false;

  //$scope.records = $meteor.collection(Meteor.users, false).subscribe('records');

  $scope.start = function () {
    $scope.explorerStarted = true;
  }

  $scope._get_new_times_dict = function () {
    var date_zero = new Date(2015, 1, 1, 0, 0, 0, 0);
    return {
        //'avg' : date_zero,//moment.duration({hours:0, minutes:0, seconds:0, milliseconds:0}),
        //'high' : date_zero,//moment.duration({hours:0, minutes:0, seconds:0, milliseconds:0}),
        //'low' : new Date(2015, 1, 1, 0, 99, 0, 0),//moment.duration({hours:0, minutes:99, seconds:0, milliseconds:0}),
        'avg' : moment.duration({hours:0, minutes:0, seconds:0, milliseconds:0}),
        'high' : moment.duration({hours:0, minutes:0, seconds:0, milliseconds:0}),
        'low' : moment.duration({hours:0, minutes:99, seconds:0, milliseconds:0}),
        'n':0
    }
  }
  // USED for FUTURE when we use javascript Date() obj for charting
  $scope._getSwimDateTime = function (swim_time) {
    var d = new Date(2015, 1, 1);
    // FORMAT: "00:00:00.00" // hh:mm:ss:SS
    var time_string_array = swim_time.split(":");
    d.setMinutes(time_string_array[1] );
    d.setSeconds(time_string_array[2].split(".")[0] );
    d.setMilliseconds(time_string_array[2].split(".")[1]);
    console.log(time_string_array);
    console.log(d);
    return d;
  }

  $scope.processRecords = function ( records ) {
    $scope.records = records;

    // find avg, highest, and lowest times in this event
    var team_times = {
        'Overall' : $scope._get_new_times_dict()
    }

    for (var i = 0; i < records.length; i++) {
      var record = records[i];

      // Query Minimongo for team obj
      var team = $meteor.object(Teams, {team_code:record.team_code}, false);
      // Find & add team if not listed
      if ($scope.teams.indexOf(team) == -1 ) {
        //var team = Teams.find( {team_code:record.team_code} );
        //var team = $scope.meteorObject(Teams, {team_code:record.team_code}, false);
        $scope.teams.push(team);
        team_times[team.short_name] = $scope._get_new_times_dict();
      };
      // Add init deltas to times dict for team
      if (! team_times.hasOwnProperty(team.short_name)) {
        //times[team.short_name] = $scope._get_new_times_dict();

      }

      // convert swim_time to TimeDelta
      var td = moment.duration(record.swim_time); //$scope._getSwimDateTime(record.swim_time);

      // add to OVERALL
      team_times['Overall']['avg'].add(td); // ADD method for MomentJS []
      team_times['Overall']['n'] += 1 ;
      if (td > team_times['Overall']['high']) {
          team_times['Overall']['high'] = td ;
      } else if (td <= team_times['Overall']['low'] ) {
          team_times['Overall']['low'] = td ;
      }

      // add to team_code
      team_times[team.short_name]['avg'].add(td); // ADD method for MomentJS
      team_times[team.short_name]['n'] += 1 ;
      if (td > team_times[team.short_name]['high'] ) {
          team_times[team.short_name]['high'] = td ;
      }
      if (td <= team_times[team.short_name]['low'] ) {
          team_times[team.short_name]['low'] = td ;
      }
    }
    //console.log($scope.teams);
    //console.log($scope.records);
    console.log(team_times);

    $scope.updateChart(team_times, $scope.formData.time );
  }

  /** UPDATE CHART !!
    team_times: JSON  of teams & their hi, low, & avg times
    you : Time user input
  */
  $scope.updateChart = function ( team_times, you ) {
    // Data for Table
    var table = {
       'cols': [
          { id: 'team', label: 'Team', type:'string'},
          { id: 'hi', label: 'Slowest', type:'number'}, //CHANGE TO DATE for future
          { id: 'lo', label: 'Best Time', type:'number'},//CHANGE TO DATE for future
          { id: 'avg', label: 'Average Time', type:'number'},//CHANGE TO DATE for future
          //{ id: 'you', label: 'You', type:'number'}
       ],
       'rows' : []
    };

    // Data for Avg Graph
    var avg_graph = {
      'cols': [
         { id: 'team', label: 'Team', type:'string'},
         { id: 'avg', label: 'Average Time', type:'number'},//CHANGE TO DATE for future
         //{ id: 'you', label: 'You', type:'number'}
      ],
      'rows' : []
    };

    // BUILD DATA & CALCULATE AVGS
    var low_y = 900; // lowest time
    for (var team_name in team_times) {
        var team = team_times[team_name];

        // calcualte avg
        if (team.n > 1) {
          // create new duration based off previous milliseconds for avg
          team.avg = moment.duration(team.avg.asMilliseconds() / team.n );
        }
        // ROW for Table
        var table_row = {
          c: [
            {v: team_name},
            {v: team.high.asSeconds()}, // asSeconds is MomentJS
            {v: team.low.asSeconds()}, // asSeconds is MomentJS
            {v: team.avg.asSeconds()}, // asSeconds is MomentJS
          ]
        };
        var avg_row = {
          c: [
            { v: team_name },
            { v: team.avg.asSeconds()}, // asSeconds is MomentJS
          ]
        };
        table['rows'].push( table_row );
        avg_graph['rows'].push( avg_row );

        if (team.low < low_y) {
          low_y = team.low;
        }
    }
    // ADD YOU row
    avg_graph['rows'].push( { c : [
      {v: "You"},
      {v: you },
    ]}) ;

    // TABLE CHART DEF
    $scope.tableChart = {
      type : "Table",
      //type: 'google.charts.Table',
      options : {
        title: 'Suggested Teams',
        alternatingRowStyle: true,
        showRowNumber: true,
        hAxis: {
          //title: 'Time',
          //format: 'mm:ss:SS',
        },
      },
      data : table,
    };

    // AVERAGE CHART DEF
    $scope.avgChart = {
      //type: "BarChart",
      type: 'google.charts.Bar', // MATERIAL
      options : {
        chart : {
          title: " Average Time per Team",
          subtitle: "subtitle",
        },
        //fill: 20,
        //displayExactValues: true,
        bars: 'horizontal', // Required for Material Bar Charts
        vAxis: {title: 'Team'},
        hAxis: {
          title: 'Average Time',
          //format: 'mm:ss:SS',
        },
        legend: { position: "none" },
        //series: {2: {type: 'line'}},
        width: 600,
        height: 600,
      },
      data: avg_graph
    };
    //$scope.avgChart.options = google.charts.Bar.convertOptions($scope.avgChart.options);

    // Success?!
    if (table['rows'].length == 1 ) {
        $scope.message = "No records found." ;
        $scope.searched = false;
    } else {
        $scope.searched = true;
    }
  }


  // function to process the form
  $scope.postResults = function () {
    $scope.loading=true;
    var valid_msg = $scope.validateFormData($scope.formData ) ;
    if ( valid_msg.indexOf("yes") > -1) {
      // Get(Query) records from server into local/client Minimongo
      // THEN 'query' client Minimongo for all Records
      var subscription = $meteor.subscribe('recordsByFormData', $scope.formData);
      subscription.then(function() {
        // Process ALL RECORDS in local DB
        $scope.processRecords( $meteor.collection(Records) );
        // Done loading when records processed
        $scope.loading=false;
      });

    } else {
      $scope.loading=false;
      console.log(valid_msg);
    }
  }

  $scope.validateFormData = function (formData ) {
    // validate time input
    return 'yes';
  }

  $scope.exploreAgain = function () {
    $scope.searched = false;
    $scope.avgChart = null;
    $scope.tableChart = null;
    //$scope.$apply();
    //$location.path('/explorer/times');
  }

  $scope.states = [ 'Alaska',
        'Arizona',
        'Arkansas',
        'California',
        'Colorado',
        'Connecticut',
        'Delaware',
        'Florida',
        'Georgia',
        'Hawaii',
        'Idaho',
        'Illinois',
        'Indiana',
        'Iowa',
        'Kansas',
        'Kentucky',
        'Louisiana',
        'Maine',
        'Maryland',
        'Massachusetts',
        'Michigan',
        'Minnesota',
        'Mississippi',
        'Missouri',
        'Montana',
        'Nebraska',
        'Nevada',
        'New Hampshire',
        'New Jersey',
        'New Mexico',
        'New York',
        'North Carolina',
        'North Dakota',
        'Ohio',
        'Oklahoma',
        'Oregon',
        'Pennsylvania',
        'Rhode Island',
        'South Carolina',
        'South Dakota',
        'Tennessee',
        'Texas',
        'Utah',
        'Vermont',
        'Virginia',
        'Washington',
        'West Virginia',
        'Wisconsin',
        'Wyoming'
    ];
}
