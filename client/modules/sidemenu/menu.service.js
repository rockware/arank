(function(){

  'use strict';

  angular.module('app.sidemenu')
    .factory('MenuService', ['$location', function ($location) {
        var sections = [
          { name: 'Home',
            state: 'core.home',
            type: 'link' },
          { name: 'Explorer',
            state: 'core.explorer.times',
            type: 'link' },
        ];

        sections.push({
          name: 'NCAA Teams',
          type: 'toggle',
          pages: [{
            name: 'I',
            type: 'link',
            state: 'core.teams',
            icon: 'univ'
          }, {
            name: 'II',
            state: 'core.teams',
            type: 'link',
            icon: 'fa fa-map-marker'
          },
            {
              name: 'III',
              state: 'core.teams',
              type: 'link',
              icon: 'fa fa-plus'
            }]
        });

        sections.push({
          name: 'FAQ',
          state: 'core.faq',
          type: 'link',
        });


        var self;

        return self = {
          sections: sections,

          toggleSelectSection: function (section) {
            self.openedSection = (self.openedSection === section ? null : section);
          },
          isSectionSelected: function (section) {
            return self.openedSection === section;
          },

          selectPage: function (section, page) {
            page && page.url && $location.path(page.url);
            self.currentSection = section;
            self.currentPage = page;
          }
        };
      }])

})();
