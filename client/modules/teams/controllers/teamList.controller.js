'use strict';
/**
 * @ngdoc function
 * @name dashboard.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the dashboard
 */
angular.module('app.explorer')
  .controller('TeamListCtrl', ['$scope', '$rootScope', '$meteor', '$location', 'TeamService', TeamListCtrl ])
  ;


function TeamListCtrl($scope, $rootScope, $meteor, $location, TeamService) {
  $scope.loading = true;
  $scope.division = "I";

  // TO-DO: IMPLEMENT PAGING
  this.subscription = $meteor.subscribe('teams');
  this.subscription.then (function () {
    $scope.teams = $meteor.collection(Teams);
    // Done loading when records processed
    $scope.loading=false;
  });


}
