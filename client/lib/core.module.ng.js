(function() {
    'use strict';

    angular
        .module('app.core', [
          'angular-meteor',
          //'oc.lazyLoad',
          'ui.router',
          'angularUtils.directives.dirPagination',
          //'uiGmapgoogle-maps',
          'ngMaterial',
          //'googlechart'
          //'chart.js'
          //'ngCookies'
        ]);
})();
